package us.nateyoung.delaycommand.runnable;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

public class RunCommandLater extends BukkitRunnable {

	private String command;
	
	public RunCommandLater(String command) {
		this.command = command;
	}
	
	@Override
	public void run() {
		
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
		
	}
	
}
