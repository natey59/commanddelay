package us.nateyoung.delaycommand;

import org.bukkit.plugin.java.JavaPlugin;

import us.nateyoung.delaycommand.CommandExecutors.Delay;

public class CommandDelay extends JavaPlugin {

	@Override
	public void onEnable() {
		
		this.saveDefaultConfig();
		
		this.getCommand("delay").setExecutor(new Delay(this));
		
	}
	
}
