package us.nateyoung.delaycommand.CommandExecutors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import us.nateyoung.delaycommand.CommandDelay;
import us.nateyoung.delaycommand.permission.Permissions;
import us.nateyoung.delaycommand.runnable.RunCommandLater;

public class Delay implements CommandExecutor {
	private CommandDelay plugin;

	public Delay(CommandDelay instance) {
		this.plugin = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender.hasPermission(Permissions.useDelay)) {
			List<String> argsList = new ArrayList<String>();
			String delayedCommand = "";
			
			argsList = Arrays.asList(args);
						
			for (String part : argsList) {
				
				delayedCommand = delayedCommand + part + " ";
				
			}
			
			RunCommandLater rcl = new RunCommandLater(delayedCommand);
			
			Long delay = plugin.getConfig().getLong("delay")*20;
			
			rcl.runTaskLater(plugin, delay);
			
			
		} else {
			sender.sendMessage(ChatColor.RED
					+ "Sorry, but you do not have permission to use this command. "
					+ "If you believe this to be in error please feel free to contact any server admin." );
		}

		return true;
	}

}
